from typing import Dict


class Expense:

    def __init__(self, source: str, account: str, name: str, category: str, subcategory: str, value: float,
                 close_date: str, open_date: str):
        self.source = source
        self.account = account
        self.name = name
        self.category = category
        self.subcategory = subcategory
        self.value = value
        self.close_date = close_date
        self.open_date = open_date

    def serialize(self) -> Dict:
        return {
            'source': self.source,
            'account': self.account,
            'category': self.category,
            'subcategory': self.subcategory,
            'name': self.name,
            'value': self.value,
            'close_date': self.close_date,
            'open_date': self.open_date,
            'hash': self.__hash__(),
        }

    def __hash__(self):
        return hash((self.source, self.account, self.name, self.value, self.close_date, self.open_date))

    def __eq__(self, other):
        if isinstance(other, Expense):
            return self.__hash__() == other.__hash__()
        return False

    @property
    def hash(self) -> int:
        return self.__hash__()

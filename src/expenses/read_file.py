import logging
import os
from abc import ABC, abstractmethod
from datetime import datetime
from typing import Dict

import xlrd
from openpyxl import load_workbook

from src.categorize.services import Categorize
from src.expenses.entities import Expense

DATE_FORMAT = '%Y-%m-%d'


class ReadFile(ABC):
    _expenses: Dict[int, Expense] = {}

    def __init__(self, path: str):
        self.path = path

    def _list_files(self):
        for f in os.listdir(self.path):
            f_path = os.path.join(self.path, f)
            if os.path.isfile(f_path):
                yield f_path

    @property
    def expenses(self):
        return self._expenses

    def uncategorized_names(self):
        return [f' [{expense.close_date} | {expense.value}] {expense.name}' for expense in self._expenses.values() if
                expense.category == 'Other' and expense.subcategory == 'Uncategorized']

    @abstractmethod
    def read(self, categorize: Categorize) -> None:
        """Read file and return a list of Expense"""


class ReadOpenBank(ReadFile):
    def __init__(self, path: str):
        super().__init__(path=path)
        self._expenses = {}

    def read(self, categorize: Categorize) -> None:
        logging.info(f"Reading files from {self.path}")
        for f in self._list_files():
            file_name = f.split(os.sep)[-1]
            logging.debug(f"Reading file {f}, file name {file_name}")
            if file_name.startswith('.'):
                continue
            book = load_workbook(f)
            sheet = book.active
            account_number = ''

            for index, row in enumerate(sheet.iter_rows(min_row=1, values_only=True)):
                if index == 3:
                    account_number = row[3]
                    logging.debug(f"Reading file {file_name}, account number {account_number}")
                    continue
                elif index < 11:
                    continue
                values = [cell for cell in row if cell is not None]
                if len(values) == 0:
                    continue
                logging.debug(f"Processing row {values}")
                category, subcategory = categorize.classify(values[2])
                close_date = values[0].strftime(DATE_FORMAT)
                open_date = values[1].strftime(DATE_FORMAT)
                _expense = Expense(source='open bank',
                                   account=account_number,
                                   name=values[2],
                                   category=category,
                                   subcategory=subcategory,
                                   value=float(values[3]),
                                   close_date=close_date,
                                   open_date=open_date)
                if _expense.hash in self._expenses:
                    continue
                self._expenses[_expense.hash] = _expense


class ReadWizink(ReadFile):
    def __init__(self, path: str):
        super().__init__(path=path)
        self._expenses = {}

    def read(self, categorize: Categorize) -> None:
        """Read file and return a list of Expense

        Returns:
            List[Expense]
        """
        logging.info(f"Reading files from {self.path}")
        for f in self._list_files():
            file_name = f.split(os.sep)[-1]
            logging.debug(f"Reading file {f}, file name {file_name}")
            if file_name.startswith('.'):
                continue
            book = xlrd.open_workbook(f)
            sheet = book.sheet_by_index(0)
            account_number = '... 9469'

            for index in range(sheet.nrows):
                if index == 0:
                    continue
                row = sheet.row(index)
                values = [cell.value for cell in row if cell.value is not None]
                if len(values) == 0:
                    continue
                logging.debug(f"Processing row {values}")
                category, subcategory = categorize.classify(values[2])
                close_date = datetime.strptime(values[0], '%d/%m/%Y').strftime(DATE_FORMAT) if values[0] else None
                _expense = Expense(source='wizink',
                                   account=account_number,
                                   category=category,
                                   subcategory=subcategory,
                                   name=values[2],
                                   value=-1 * float(values[4]),
                                   close_date=close_date,
                                   open_date=close_date)
                if _expense.hash in self._expenses:
                    continue
                self._expenses[_expense.hash] = _expense


class ReadBbva(ReadFile):
    def __init__(self, path: str):
        super().__init__(path=path)
        self._expenses = {}

    def read(self, categorize: Categorize) -> None:
        logging.info(f"Reading files from {self.path}")
        for f in self._list_files():
            file_name = f.split(os.sep)[-1]
            logging.debug(f"Reading file {f}, file name {file_name}")
            if file_name.startswith('.'):
                continue
            book = load_workbook(f)
            sheet = book.active
            account_number = 'ES1901824000670201806818'

            for index, row in enumerate(sheet.iter_rows(min_row=1, values_only=True)):
                if index < 5:
                    continue
                values = [cell for cell in row if cell is not None]
                if len(values) == 0:
                    continue
                logging.debug(f"Processing row {values}")
                category, subcategory = categorize.classify(values[2])
                _expense = Expense(source='bbva', account=account_number, category=category, subcategory=subcategory,
                                   name=values[2], value=float(values[4]), close_date=values[0].strftime(DATE_FORMAT),
                                   open_date=values[1].strftime(DATE_FORMAT))
                if _expense.hash in self._expenses:
                    continue
                self._expenses[_expense.hash] = _expense

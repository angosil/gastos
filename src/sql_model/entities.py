from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class TableCategorize(Base):
    __tablename__ = 'categorized'

    id = Column(String(32), primary_key=True)
    pattern = Column(String(256))
    category = Column(String(128))
    sub_category = Column(String(128))

    def __repr__(self):
        return f"<TableCategorize(pattern={self.pattern}, category='{self.category}, sub_category={self.sub_category}')>"

import logging
from typing import Tuple, Dict, List

MAPPING_CATEGORY: Dict[Tuple, List] = {
    ('market', 'food'): ['mercadona', 'carrefour', 'carref', 'alcampo', 'dia', 'ahorramas', 'aldi', 'eroski',
                         'supersol', 'hipercor', 'costco', 'alimentacion bazar', 'lidl', 'alimentacion zhang xiaohu'],
    ('extracurricular', 'pool'): ['adeudo ayuntamiento de madrid'],
    ('extracurricular', 'musica'): ['recibo escuela de musica'],
    ('school', 'payment'): ['recibo gredos san diego', 'recibo fundacion gsd', 'compra en gredos san diego'],
    ('income', 'transfer'): ['transferencia recibida', 'transferencia de'],
    ('income', 'salary'): ['sotec consulting'],
    ('income', 'bizum'): ['bizum de'],
    ('income', 'refund'): ['bonificacion sobre el importe'],
    ('payment', 'transfer'): ['transferencia realizada', 'transferencia a favor de'],
    ('payment', 'foot'): ['panaderia', 'burger king', 'heladeria roma', 'africa fusion', 'haagen dazs', 'ginos',
                          'mcdonald', 'granier benidorm'],
    ('payment', 'clothes'): ['compra en klarna', 'clarks outlet'],
    ('payment', 'rental'): ['recibo agencia negociadora'],
    ('payment', 'bizum'): ['bizum a favor'],
    ('payment', 'taxes'): ['retencion hacienda', 'impuesto'],
    ('payment', 'consignment'): ['compra en small', 'small world'],
    ('payment', 'sopping'): ['amzn mktp', 'amazon es', 'www amazon', 'amz stock', 'compra en bazar', 'li da superestrellas'],
    ('payment', 'donation'): ['hillsong es', 'recibo fundacion unicef', 'recibo cruz roja', 'recibo acnur'],
    ('payment', 'transportation'): ['taxi lic', 'compra en taxi'],
    ('payment', 'security'): ['recibo securitas direct'],
    ('payment', 'google'): ['compra en google cloud'],
    ('payment', 'apple'): ['compra en apple com'],
    ('entertainment', 'balls park'): ['pulumparque', 'compra en fec coslada', 'bolera plenilunio'],
    ('entertainment', 'cinema'): ['compra en cinesa'],
    ('entertainment', 'theater'): ['sala galileo galilei'],
    ('entertainment', 'parck'): ['compra en faunia'],
    ('health', 'pharmacy'): ['farmacia'],
    ('health', 'insurance'): ['recibo sanitas'],
    ('car', 'parking'): ['bkt mad park', 'parking', 'aparcamiento'],
    ('car', 'insurance'): ['recibo mutua madrilena'],
    ('cash', 'withdrawal'): ['disposicion en cajero'],
    ('utilities', 'weather'): ['recibo canal de isabel ii'],
    ('utilities', 'electricity and gas'): ['recibo endesa energia', 'recibo energygo', 'recibo pepe energy'],
    ('utilities', 'internet'): ['recibo pepe mobile'],
    ('amortization', 'debt'): ['pago recibo mes anterior', 'liquidacion periodica prestamo', 'compra en oney',
                               'recibo wizink bank', 'recibo banco cetelem', 'recibo caixabank', 'recibo financiera'],
    ('amortization', 'interest'): ['intereses'],
    ('amortization', 'credict card'): ['liquidacion de la tarjeta'],
    ('investment', 'etoro'): ['compra en etoro'],
}


class Categorize:
    def __init__(self, category_mapping):
        self.category_mapping = category_mapping

    def classify(self, transaction_name) -> Tuple[str, str]:
        logging.info(f"Classifying transaction {transaction_name}")
        transaction_name = self.preprocess(transaction_name)
        for category_subcategory, keywords in self.category_mapping.items():
            for keyword in keywords:
                if keyword in transaction_name:
                    logging.info(f"Transaction {transaction_name} classified as {category_subcategory}")
                    return category_subcategory
        logging.warning(f"Transaction {transaction_name} not classified")
        return 'Other', 'Uncategorized'

    @staticmethod
    def preprocess(name: str) -> str:
        name = name.lower().replace('-', ' ').replace('.', ' ').replace('*', ' ')
        name = name.split()
        name = ' '.join(name)
        return name


def classifier() -> Categorize:
    return Categorize(MAPPING_CATEGORY)

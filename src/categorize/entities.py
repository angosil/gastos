import uuid
from typing import Dict


class Categorize:
    def __init__(self, id: str, pattern: str, category: str = None, subcategory: str = None):
        self.id = id
        self.pattern = pattern
        self.category = category
        self.subcategory = subcategory

    @classmethod
    def first(cls, pattern: str, category: str = None, subcategory: str = None):
        return cls(uuid.uuid4().hex, pattern, category, subcategory)

    def serialize(self) -> Dict:
        return {
            'id': self.id,
            'pattern': self.pattern,
            'category': self.category,
            'subcategory': self.subcategory,
        }

    def __repr__(self):
        return f"<Category(pattern={self.pattern}, category='{self.category}, subcategory={self.subcategory}')>"

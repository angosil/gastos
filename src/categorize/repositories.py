from abc import ABC
from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker

from src.categorize.entities import Category
from src.config import Config


class SQLCategoryzeRepository(ABC):
    def __init__(self):
        engine = create_engine(Config.SQLALCHEMY_DATABASE_URI, echo=True)
        Session = sessionmaker(bind=engine)
        self.session = Session()



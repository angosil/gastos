import logging
import os

from src.categorize.services import classifier

root = logging.getLogger()
root.setLevel(logging.DEBUG)

# handler = logging.StreamHandler(sys.stdout)
handler = logging.FileHandler('debug.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

from src.expenses.read_file import ReadWizink, ReadOpenBank, ReadBbva

OPEN_BANK_PATH = os.path.join(os.path.dirname(__file__), 'source', 'open_bank')
WIZINK_PATH = os.path.join(os.path.dirname(__file__), 'source', 'wizink')
BBVA_PATH = os.path.join(os.path.dirname(__file__), 'source', 'bbva')

if __name__ == "__main__":

    read_open_bank = ReadOpenBank(OPEN_BANK_PATH)
    read_open_bank.read(classifier())
    # for expense in read_open_bank.expenses:
    #     print(expense.serialize())

    read_wizink = ReadWizink(WIZINK_PATH)
    read_wizink.read(classifier())
    # for expense in read_wizink.expenses:
    #     print(expense.serialize())

    read_bbva = ReadBbva(BBVA_PATH)
    read_bbva.read(classifier())
    # for expense in read_bbva.expenses.values():
    #     print(expense.serialize())

    for name in read_open_bank.uncategorized_names():
        print(name)
    for name in read_wizink.uncategorized_names():
        print(name)
    for name in read_bbva.uncategorized_names():
        print(name)
